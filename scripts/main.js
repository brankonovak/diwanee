/**
 * Global namespace
 */
var APP = APP || {};


(function () {
	var $body = $('body'),
		$navItems = $(".js-nav-links li"),
		$navFooterLeft = $('.js-nav-footer-left'),
		$navFooterRight = $('.js-nav-footer-right li');

	$('.js-nav-icon').on('click', function () {
		$body.toggleClass('css-js-active-nav');

		if ($body.hasClass('css-js-active-nav')) {
			$navItems.velocity("transition.slideUpIn", { stagger: 50 });
			$navFooterLeft.velocity("transition.slideLeftIn", { stagger: 50 });
			$navFooterRight.velocity("transition.slideRightIn", { stagger: 50 });
		}
	});	
}());